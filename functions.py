import os, random, time
from datetime import datetime

usuarios = [
    {
        "nombre": "Kevin Peña Salvatierra",
        "nif": "12345678-BAR",
        "nacionalidad": "España",
        "edad": 25,
        "salario": 3000,
        "fecha_nacimiento": "07-08-1998",
        "estado_conyugal": "SOLTERO",
    }
]

ciudades = {
    "barcelona": "BAR",
    "madrid": "MAD",
    "sevilla": "SEV",
    "valencia": "VAL",
    "bilbao": "BIL",
    "zaragoza": "ZAR",
    "toledo": "TOL",
}


def app():
    while True:
        os.system("cls")
        print(
            f"""Sistema Piloto
        1) Registrar usuario
        2) Buscar usuario
        3) Imprimir Certificado
        4) Salir
            """
        )
        print(usuarios)
        try:
            menuOption = int(input("Ingrese una opción: "))

            if menuOption == 1:
                registrarUsuario()

            elif menuOption == 2:
                print("Buscar usuario")
                nif_a_buscar = input("Ingrese el NIF a buscar: ")
                print("Buscando...")
                time.sleep(1.5)

                usuarioEncontrado = buscarUsuarioPorNIF(nif_a_buscar, usuarios)
                if usuarioEncontrado:
                    os.system("cls")
                    imprimirInfo(0, usuarioEncontrado)
                    input("\nEnter para continuar...")
                else:
                    print("No existe el usuario con ese NIF.")
                    time.sleep(1.2)

            elif menuOption == 3:
                while True:
                    try:
                        print("Certificados")
                        print("1) Certificado nacimiento\n2) Certificado estado conyugal\n3) Certificado salario mensual\n4) Volver")
                        certificado_option = int(input("Ingrese una opción: "))
                        if certificado_option in [1,2,3,4]:
                            #Para volver al menu si ingresa la opcion 4.
                            if certificado_option == 4:
                                break
                            
                            nif_a_buscar = input("Ingrese el NIF a buscar: ")
                            print("Buscando...")
                            time.sleep(1.5)

                            usuarioEncontrado = buscarUsuarioPorNIF(nif_a_buscar, usuarios)
                            if usuarioEncontrado:
                                os.system("cls")
                                imprimirInfo(certificado_option, usuarioEncontrado)
                                input("\nEnter para continuar...")
                            else:
                                print("No existe el usuario con ese NIF.")
                                time.sleep(1.2)
                        else:
                            print("Ingrese una opción válida.")

                    except:
                        print("Opcion invalida")
                        
            elif menuOption == 4:
                print("Saliendo del sistema...")
                time.sleep(1.5)
                break
            else:
                print("No existe esa opción.")
                time.sleep(0.5)

        except ValueError:
            print("Ingrese un número válido.")
            print(ValueError)
            time.sleep(1)


def registrarUsuario():
    os.system("cls")
    print("Registro")
    # Primero pedimos la ciudad
    city = inputCity()

    # Luego de obtener la ciudad, la enviamos como parametro a la funcion que crea el nif final
    nif = inputNIF(city)
    nombre = inputNombre()
    edad = inputEdad()

    # Generamos la información restante para los certificados...
    salario_mensual = random.randint(1000, 10000)
    estado_conyugal = random.choice(["SOLTERO", "CASADO", "DIVORCIADO"])
    fecha_nacimiento = generarFechaNacimiento(edad)

    usuarios.append(
        {
            "nombre": nombre,
            "nif": nif,
            "nacionalidad": "España",
            "edad": edad,
            "salario": salario_mensual,
            "estado_conyugal": estado_conyugal,
            "fecha_nacimiento": fecha_nacimiento,
        }
    )
    
#Recibimos la edad para calcular el año de nacimiento
def generarFechaNacimiento(edad):
    año_actual = 2024
    año_nacimiento = año_actual - edad
    mes = random.randint(1, 12)

    # Determinar el número máximo de días para el mes seleccionado
    if mes in [4, 6, 9, 11]:
        max_dias = 30
    else:
        max_dias = 31

    # Generar un día aleatorio
    dia = random.randint(1, max_dias)

    # Crear la fecha de nacimiento
    fecha_nacimiento = datetime(año_nacimiento, mes, dia)

    # Formatear la fecha como string (puedes ajustar el formato según tus necesidades)
    return fecha_nacimiento.strftime("%d-%m-%Y")


def inputCity():
    while True:
        city = input("Ingrese la ciudad: ")
        if city.lower() in ciudades:
            return city
        else:
            print(
                f"Debe ingresar una de las siguientes ciudades: {', '.join([c.capitalize() for c in ciudades.keys()])}"
            )


# Recibe como parametro la ciudad, ya que necesita sacar el codigo de la ciudad (ver el dict ciudades{}) para crear el NIF final.
def inputNIF(city):
    # Ciclo para validar NIF
    while True:
        # Para pedir el nif por teclado....
        # nif = input("Ingrese los números del NIF: ")

        # Para generar el NIF aleatoriamente...
        nif = ""
        for i in range(8):
            nif += str(random.randint(0, 9))
        print(nif)

        # Verificamos que solo hayan números en el string y que su largo sea 8.
        if nif.isdigit() and len(nif) == 8:
            # Creamos el NIF final agregando el codigo de la ciudad como indica el formato del ejercicio.
            nif = f"{nif}-{ciudades[city.lower()]}"
            # Verificamos que el nif no esté asignado a otro usuario.
            if existeNIF(nif, usuarios):
                print("El NIF ya está registrado, ingrese números distintos.")
            else:
                return nif
        else:
            if not nif.isdigit():
                print("Deben ser solo valores numericos.")
            else:
                print("Debe tener 8 caracteres de largo.")


# Funcion que busca si el nif ya está registrado a un usuario en la lista de usuarios.
def existeNIF(nif, usuarios):
    for usuario in usuarios:
        if usuario["nif"] == nif:
            return True
    return False


def buscarUsuarioPorNIF(nif, usuarios):
    for usuario in usuarios:
        if usuario["nif"] == nif:
            return usuario
    return None


def inputNombre():
    while True:
        nombre = input("Ingrese el nombre: ")
        if len(nombre.strip()) > 0:
            if len(nombre.strip()) >= 8:
                return nombre
            else:
                print("El nombre debe tener minimo 8 caracteres.")
        else:
            print("El nombre no puede estar vacío.")


def inputEdad():
    while True:
        try:
            edad = int(input("Ingrese la edad: "))
            if edad <= 0:
                print("La edad debe ser mayor que 0.")
            elif edad <= 15:
                print("La edad debe ser mayor que 15.")
            else:
                return edad
        except:
            print("La edad debe ser un número")


def imprimirInfo(tipo, usuario):
    # Tipo para saber si mostrar la info normal, o la de certificados, asi aprovechamos la misma funcion.
    info = f"""
        ---------- Datos del Usuario
        NIF: {usuario["nif"]}
        Nombre: {usuario["nombre"]}
        Edad: {usuario["edad"]}
        Nacionalidad: {usuario["nacionalidad"]}
          """
    if tipo == 0: 
        print(info)
        
    elif tipo == 1:
        print(
            f""" ------------- Certificado de nacimiento
            Fecha nacimiento: {usuario["fecha_nacimiento"]}
            """ + info
        )
    elif tipo == 2:
        print(
            f""" ------------- Certificado de estado conyugal
            Estado conyugal: {usuario["estado_conyugal"]}
            """ + info
        )
    elif tipo == 3:
        print(
            f""" ------------- Certificado de salario mensual
            Salario mensual: {usuario["salario"]}
            """ + info
        )
